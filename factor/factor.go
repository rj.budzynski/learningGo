package main

import (
	"fmt"
	"os"
	"strconv"
)

func getFactors(n uint64) []uint64 {
	a := make([]uint64, 0)
	for k := uint64(2); k*k <= n; k++ {
		for n%k == 0 {
			n = n / k
			a = append(a, k)
		}
	}
	if n != 1 {
		a = append(a, n)
	}

	return a
}

func main() {
	for _, arg := range os.Args[1:] {
		n, err := strconv.ParseUint(arg, 10, 64)
		if err != nil {
			fmt.Printf("%s: is not a positive integer\n", arg)
			continue
		}
		factors := getFactors(uint64(n))
		fmt.Printf("%d: ", n)
		for _, f := range factors {
			fmt.Print(f, " ")
		}
		fmt.Println()
	}
}
