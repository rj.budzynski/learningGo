package main

import (
	"fmt"
	"os"
	"strconv"
)

func getFactors(n uint64, c chan []uint64) {
	a := make([]uint64, 0)
	for k := uint64(2); k*k <= n; k++ {
		for n%k == 0 {
			n = n / k
			a = append(a, k)
		}
	}
	if n != 1 {
		a = append(a, n)
	}
	c <- a
}

func main() {
	args := os.Args[1:]
	num := len(args)
	if num == 0 {
		return
	}
	channels := make([]chan []uint64, num, num)
	numbers := make([]uint64, num, num)
	for i, arg := range args {
		n, err := strconv.ParseUint(arg, 10, 64)
		if err != nil {
			continue
		}
		c := make(chan []uint64)
		channels[i] = c
		numbers[i] = n
		go getFactors(n, c)
	}
	for i, c := range channels {
		if c != nil {
			fmt.Printf("%d: ", numbers[i])
			for _, f := range <-c {
				fmt.Printf("%d ", f)
			}
			fmt.Println()
		} else {
			fmt.Printf("%s: is not a positive integer\n", args[i])
		}
	}
}
