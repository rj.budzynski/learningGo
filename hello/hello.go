// the usual first code

package main

import (
	"fmt"
	"math"
	"os"
	"time"
)

func daysSince(t0 time.Time) int {
	dur := time.Since(t0)
	days := int(math.Floor(dur.Hours() / 24))
	return days
}

func main() {
	user := os.Getenv("USER")
	fmt.Printf("*\n* Hello user %s!\n", user)
	fmt.Println("* The time is now:", time.Now().Format("3 PM 4 min"))
	fmt.Println("* Today is", time.Now().Format("January 2, 2006"))
	days := daysSince(time.Unix(0, 0))
	fmt.Printf("* It is %d days since the start of the Unix era.\n*\n", days)
	bday, _ := time.Parse("02-01-2006 15:04 -0700", "14-07-1959 22:10 +0100")
	days = daysSince(bday)
	fmt.Printf("* It is now %d days since the author of this code was born.\n*\n", days)
}
