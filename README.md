# simple exercises in golang

## hello

the usual beginner code, plus an excursion into formatting and parsing datetimes

## factor

brute force prime factorization of integers

## factor2

as above, but implemented with a closure

## factor 3

parallelized with goroutines and channels

## TODO:

have factorization switch to bigints when required

----
comments? <robert@budzynski.xyz>