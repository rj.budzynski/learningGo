package main

import (
	"fmt"
	"os"
	"strconv"
)

func factors(n uint) func() uint {
	k := uint(2)
	return func() uint {
		for ; k*k <= n; k++ {
			if n%k == 0 {
				n = n / k
				return k
			}
		}
		n, k = 1, n
		return k
	}
}

func main() {
	for _, arg := range os.Args[1:] {
		n, err := strconv.ParseUint(arg, 10, 64)
		if err != nil {
			fmt.Printf("%s: is not a positive integer\n", arg)
			continue
		}
		fmt.Printf("%d: ", n)
		nextFactor := factors(uint(n))
		k := nextFactor()
		for k != 1 {
			fmt.Printf("%d ", k)
			k = nextFactor()
		}
		fmt.Println()
	}
}
